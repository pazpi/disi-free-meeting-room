# DISI Free Meeting Room

Check for a free meeting room checking the google calendar link.

## How to

This code was created starting from the example on the google-calendar API

[Follow](https://developers.google.com/google-apps/calendar/quickstart/python) this link to create the secret key for access the event.

Next install all the requirements

`pip install -r requirements.txt`

And execute with

`python main.py`

The first execution will require the oauth2 autentication so a broswer will open asking for a login with the username that created the api request

## Final note

This code was created in a boring afternoon in an hour and half so is far from perfect.

The next idea is to not only see if the room is free but the next reservation and maybe the all day schedule.

The final goal is to wrap around a Telegram bot so a user can simply write a message and instant notify when a room is free.
