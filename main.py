from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

import datetime

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'

room_name = {
    'Cornisello': 'v3q4pcc53d5h38n4j4td6bm6rs@group.calendar.google.com',
    'Terlago': 'sbm69d9hifer1glgp8g5kdbvl4@group.calendar.google.com',
    'Lamar': 'dn14m37shc6c2gvpkilv8cia40@group.calendar.google.com',
    'Molveno': 'mptfsevh0jg8fhmgp606e1gkso@group.calendar.google.com',
    'Toblino': 'andm8tv88ucp3utht91pv1ec34@group.calendar.google.com',
    'Yoram Ofek': 'vpl8l5m1qijqt6a5h06i6qf40c@group.calendar.google.com',
    'Garda': 'cosgdpd3cde6bmsav4g5or0dvc@group.calendar.google.com',
    'Lases': 'gbajrn9c6n0dvj7fhl5c6qs980@group.calendar.google.com',
    'Levico': 'bu8heo86aguck9idgo8psf7m18@group.calendar.google.com'
}


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def is_event_now(date_start, date_end):
    """ date_start, date_end = {str} """
    date_now = datetime.datetime.today()
    if date_start[-1] == 'Z':
        d_start = datetime.datetime.strptime(date_start[:-1], '%Y-%m-%dT%H:%M:%S')
    else:
        d_start = datetime.datetime.strptime(date_start[:-6], '%Y-%m-%dT%H:%M:%S')
    if date_end[-1] == 'Z':
        d_end = datetime.datetime.strptime(date_start[:-1], '%Y-%m-%dT%H:%M:%S')
    else:
        d_end = datetime.datetime.strptime(date_end[:-6], '%Y-%m-%dT%H:%M:%S')
    delta_event = d_end - d_start
    if (date_now - d_start).days == 0:
        # the event is today
        return (date_now - date_start).seconds < delta_event.seconds


def main():
    """Shows basic usage of the Google Calendar API.

    Creates a Google Calendar API service object and outputs a list of the next
    10 events on the user's calendar.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)

    for key in room_name.keys():
        now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
        # print('Getting the upcoming event for room {}'.format(key))
        events_result = service.events().list(
            calendarId=room_name[key], timeMin=now, maxResults=1, singleEvents=True,
            orderBy='startTime').execute()
        events = events_result.get('items', [])

        if not events:
            print('No upcoming events found.')
        for event in events:
            # start = event['start'].get('dateTime', event['start'].get('date'))
            # print(start, event['summary'])
            if not is_event_now(event['start']['dateTime'], event['end']['dateTime']):
                print('Aula {} is free now'.format(key))


if __name__ == '__main__':
    main()
